import { findValue } from '~/lib/findValue'
import { getOperators } from '~/lib/operators'

const handler = findValue(async () => {
	const operators = await getOperators()
	return { operators }
})

export default handler
