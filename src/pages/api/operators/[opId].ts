import { findValue } from '~/lib/findValue'
import { getOperators } from '~/lib/operators'

const handler = findValue(async req => {
	const operators = await getOperators()
	const operator = operators.find(op => op.id === req.query.opId)
	if (operator) return { operator }
})

export default handler
