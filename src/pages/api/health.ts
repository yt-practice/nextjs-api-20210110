import type { NextApiHandler } from 'next'

const handler: NextApiHandler = (req, res) => {
	res.json({ message: 'ok' })
}

export default handler
