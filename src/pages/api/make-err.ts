import { handleError, HttpError } from '~/lib/handleError'

const handler = handleError(async () => {
	throw new HttpError(400, 'hoge')
})

export default handler
