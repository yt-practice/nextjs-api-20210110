import type { GetServerSideProps } from 'next'
import { errorMessageMap } from '~/lib/handleError'

const NotFound = () => null

export default NotFound

export const getServerSideProps: GetServerSideProps = async ({ res }) => {
	const code: number = res?.statusCode || 404
	const message = errorMessageMap[code] || 'error.'
	res.setHeader('Content-Type', 'application/json; charset=utf-8')
	res.end(JSON.stringify({ message }))
	return { props: {} }
}
