const getjson = (name: string, cat = 'excel') =>
	fetch(
		`https://raw.githubusercontent.com/Kengxxiao/ArknightsGameData/master/ja_JP/gamedata/${cat}/${name}.json`,
	)
		.then<Record<string, Record<string, string>>>(r => r.json())
		.then(map =>
			Object.entries(map)
				.filter(([_, v]) => v.name && v.itemUsage)
				.map(([id, { name, itemUsage: text }]) => ({ id, name, text }))
				.sort((q, w) => q.id.localeCompare(w.id, [], { numeric: true })),
		)

let ops$: Promise<{ id: string; name: string; text: string }[]> | undefined

export const getOperators = () => {
	ops$ ||= getjson('character_table')
	return ops$
}
