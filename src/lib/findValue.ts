import type { NextApiRequest, NextApiResponse } from 'next'
import { handleError, HttpError } from './handleError'

export const findValue = <T>(
	fn: (
		req: NextApiRequest,
		res: NextApiResponse<T>,
	) => Promise<T | undefined> | T | undefined,
) =>
	handleError(async (req, res) => {
		const v = await fn(req, res)
		if (undefined !== v) return res.json(v)
		throw new HttpError(404)
	})
