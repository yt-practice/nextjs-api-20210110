const path = require('path')
const basePath = ''
const withBundleAnalyzer = require('@next/bundle-analyzer')({
	enabled: 'true' === process.env.ANALYZE,
})
module.exports = withBundleAnalyzer({
	assetPrefix: basePath,
	publicRuntimeConfig: {
		basePath,
	},
	async rewrites() {
		return [
			{
				source: '/',
				destination: '/api',
			},
			{
				source: '/:path*',
				destination: '/api/:path*',
			},
		]
	},
	webpack(config, _options) {
		config.resolve.alias = {
			...config.resolve.alias,
			'~': path.resolve(__dirname, 'src'),
		}
		// config.node = {
		// 	...config.node,
		// 	fs: 'empty',
		// 	child_process: 'empty',
		// }
		return config
	},
})
