next js で api だけ書いてみるテスト  
next.config.js で rewrites を設定すれば  
自由なパスから json が返せる  

next js はファイルベースのルーティングができるが api を作るときにそれが便利かどうかは分からない…  

## start dev

```shell
yarn
yarn dev
```

## routes

- `/`  
- `/health`  
- `/operators`  
- `/operators/:opId`  
- `/make-err` エラーが起こる  
